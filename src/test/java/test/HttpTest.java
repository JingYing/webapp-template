package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

public class HttpTest{
	@Test
	public void test1() throws UnsupportedEncodingException {
		String s="S2VuVHNl56ys5LiA5Liq5rGJ5YyW5ri45oiP6a2U56We6Iux6ZuE5Lyg5Y+R5biD5bm05Lu95Y+K5Y+C5LiO5Lq65pWw77yfCuetlOahiOaYrzXkuKrmlbDlrZc=";
		byte[] bs = Base64.decodeBase64(s);
		System.out.println(new String(bs,"utf-8"));
	}
	
	@Test
	public void test2() throws MalformedURLException, IOException {
		HttpURLConnection conn = (HttpURLConnection) new URL("http://api.shengqian66.cn/user/login.do").openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
//		conn.setRequestProperty("Content-Type", "multipart/form-data");//不行
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		OutputStream os = conn.getOutputStream();
		os.write("wifi=CU_9RZF&os=ios11.0.3&zfb=1&appver=1&channel=App+Store&imei=3A4C253B-FDBA-4724-BEA6-018F12F9EB48&model=iPhone+6S&tb=1&sign=32191827a01ec8977abe62b67ae2e09d".getBytes("ascii"));
		os.close();
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
		String l=null;
		while((l=br.readLine())!=null)
			System.out.println(l);
		br.close();
	}

}

package codegen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.mysql.jdbc.Driver;

import my.common.Util;

public class JpaCodeGen {
	
	public static void main(String[] args) throws Exception {
		JpaCodeGen gen = new JpaCodeGen();
		gen.gen(gen.parse());
		System.out.println("over...");
	}
	
	public Properties getProp() throws IOException {
		Properties prop = new Properties();
		prop.load(getClass().getResourceAsStream("config.properties"));
		return prop;
	}
	
	public List<Table> parse() throws Exception {
		Properties prop = getProp();
		String tableValue = prop.getProperty("table");
		Set<String> tagetTables = new HashSet<String>();
		
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(
				prop.getProperty("jdbcUrl"), 
				prop.getProperty("user"), 
				prop.getProperty("pwd")
				);
		if("*".equals(tableValue)) {
			PreparedStatement ps = conn.prepareStatement("SHOW TABLES");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				tagetTables.add(rs.getString(1));
			}
			rs.close();
			ps.close();
		}
		
		List<Table> toGen = new ArrayList<>();
		
		for(String t : tagetTables) {
			Table tableObj = new Table();
			tableObj.name = t;
			PreparedStatement ps = conn.prepareStatement("DESCRIBE "+t);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Column c = new Column();
				String field = rs.getString("Field");
				c.field = field;
				String type = rs.getString("Type");
				String key= rs.getString("Key");
				String extra = rs.getString("Extra");
				if("PRI".equalsIgnoreCase(key)) {
					c.isId = true;
					if("auto_increment".equals(extra)) {
						c.autoIncrement = true;
					}
				}
				if(type.startsWith("int") || type.startsWith("tinyint")) {
					c.type = Integer.class;
				} else if(type.startsWith("bigint")) {
					c.type = java.lang.Long.class;
				} else if(type.startsWith("char")||type.startsWith("varchar")||type.startsWith("text")) {
					c.type = String.class;
				} else if(type.startsWith("date") || type.startsWith("time")) {
					c.type = java.util.Date.class;
				} else if(type.equals("float")) {
					c.type = java.lang.Double.class;
				} else {
					throw new UnsupportedOperationException();
				}
				tableObj.columns.add(c);
			}
			rs.close();
			ps.close();
			toGen.add(tableObj);
		}
		conn.close();
		return toGen;
	}
	
	public void gen(List<Table> tables) throws IOException {
		Properties prop = getProp();
		String dir = prop.getProperty("genDir");
		for(Table t : tables) {
			String name = toCamelCase(t.name);
			File entity = new File(dir + "/entity/" + firstUppercase(name)+ ".java");
			Util.makeDirs(entity.getAbsolutePath());
			OutputStreamWriter entityWriter = new OutputStreamWriter(
											new FileOutputStream(entity), "utf-8");
			entityWriter.write(toEntityJava(t, prop)); 
			entityWriter.close();
			
			File dao = new File(dir + "/dao/" + firstUppercase(name)+ "Dao.java");
			Util.makeDirs(dao.getAbsolutePath());
			OutputStreamWriter daoWriter = new OutputStreamWriter(
					new FileOutputStream(dao), "utf-8");
			daoWriter.write(toDaoJava(t, prop)); 
			daoWriter.close();
		}
	}
	
	class Column {
		public Class type;
		public String field;
		public boolean isId, autoIncrement;
	}
	
	class Table {
		String name;
		List<Column> columns = new ArrayList<>();
	}
	
	private String toCamelCase(String s) {
		char[] arr = s.toCharArray();
		ByteBuffer buf = ByteBuffer.allocate(arr.length);
		boolean toUpper = false;
		for(int i=0; i<arr.length; i++) {
			if('_' == arr[i]) {
				toUpper = true;
			} else {
				if(toUpper) {
					buf.put((byte)Character.toUpperCase(arr[i]));
					toUpper = false;
				} else {
					buf.put((byte)arr[i]);
					
				}
			}
		}
		
		byte[] target = new byte[buf.position()];
		System.arraycopy(buf.array(), 0, target, 0, buf.position());
		try {
			return new String(target, "ascii");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException();
		}
	}
	
	private String firstUppercase(String s) {
		return (""+s.charAt(0)).toUpperCase() + s.substring(1, s.length());
	}
	
	private String toEntityJava(Table t, Properties prop) {
		String name = toCamelCase(t.name);
		Set<String> imports = new HashSet<>();
		imports.add("javax.persistence");
		
		StringBuilder s = new StringBuilder();
		s.append("@Entity\n");
		s.append("@Table(name=\""+t.name+"\")\n");
		s.append("public class "+firstUppercase(name)+"\t{\n");
		for(Column c : t.columns) {
			s.append("\tprivate "+ c.type.getSimpleName() + " "+ c.field + ";\n");
			imports.add(c.type.getName().substring(0, c.type.getName().lastIndexOf(".")));
		}
		s.append("\n");
		for(Column c : t.columns) {
			if(c.isId) {
				s.append("\t@Id\n");
				if(c.autoIncrement) {
					s.append("\t@GeneratedValue\n");
				}
			} else {
				s.append("\t@Column\n");
			}
			s.append(String.format("\tpublic %s get%s() {\n\t\treturn this.%s;\n\t}\n\n", 
									c.type.getSimpleName(), firstUppercase(c.field), c.field));
			s.append(String.format("\tpublic void set%s(%s %s) {\n\t\tthis.%s = %s;\n\t}\n\n", 
					firstUppercase(c.field), c.type.getSimpleName(), c.field, c.field, c.field 
					));
		}
		s.append("}");
		s.insert(0, "\n");
		for(String i : imports) {
			s.insert(0, "import " + i + ".*;\n");
		}
		s.insert(0, "\n");
		s.insert(0, "package "+prop.getProperty("basePackageName") + ".entity;\n");
		return s.toString();
	}
	
	private String toDaoJava(Table t, Properties prop) {
		String name = toCamelCase(t.name);
		String basePackageName = prop.getProperty("basePackageName");
		String baseDaoFullName = prop.getProperty("baseDaoFullName");
		StringBuilder s = new StringBuilder();
		s.append("package "+ basePackageName + ".dao;\n\n");
		s.append("import org.springframework.stereotype.Component;\n");
		s.append("import "+basePackageName+".entity." + firstUppercase(name)+";\n");
		s.append("import "+baseDaoFullName+";\n");
		s.append("\n");
		s.append("@Component\n");
		s.append(String.format("public class %sDao extends %s {\n", 
				firstUppercase(name), baseDaoFullName.substring(baseDaoFullName.lastIndexOf(".")+1)));
		s.append("}");
		return s.toString();
	}
	
	

}

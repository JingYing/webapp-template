package my.common;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class SignValid {

	public static void main(String[] args) {
		Map<String, String> par = new HashMap<>();
		par.put("b", "二");
		par.put("c", "三");
		par.put("a", "一");
		String sign = SignGen.genSign(par);
		par.put("sign", sign);
		System.out.println(SignValid.valid(par));
	}

	public static boolean valid(Map param) {
		TreeMap<String, String[]> map = new TreeMap<>(param);
		if(map.containsKey("sign")) {
			String sign = map.remove("sign")[0];
			String s = "4c20aadd9896435" + Util.paramMapToString(map, null) + "385c6454939a92b38";
			try {
				String mysign = Util.md5(s.getBytes("utf-8"));
				return mysign.equalsIgnoreCase(sign);
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		} else {
			return false;
		}
	}

}

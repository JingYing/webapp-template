package my.common;

import java.io.UnsupportedEncodingException;

import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

public class CodeUtil {
	
	public static void main(String[] args) {
		String e = encrypt("1234567890");
		System.out.println(e);
		System.out.println(decrypt("ry3NOoajY-Xe9-4rhqw_xA"));
	}
	
	private static final SecretKey PWD = Util.getAESKey("j89sKuhluVPn6pcO");
	
	public static String encrypt(String s){
		try {
			return Base64.encodeBase64URLSafeString(Util.AESEncrypt(s.getBytes("utf-8"), PWD));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String decrypt(String s) {
		byte[] bs = Base64.decodeBase64(s);
		bs = Util.AESDecrypt(bs, PWD);
		try {
			return new String(bs,"utf-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("不识别的参数:"+s);
		}
	}

}

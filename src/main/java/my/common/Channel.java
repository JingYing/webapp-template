package my.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Channel {
	Map<Integer,String> code_channel = new HashMap<>();
	Map<String,Integer> channel_code = new HashMap<>();
	
	public Channel() {
		BufferedReader reader = null;
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("channel.properties");
		try {
			reader  = new BufferedReader(new InputStreamReader(is));
			String l = null;
			while((l=reader.readLine())!=null && l.length()>0){
				String[] arr = l.split("=",2);
				int code = Integer.parseInt(arr[0], 16);
				code_channel.put(code,arr[1]);
				channel_code.put(arr[1], code);
			}
		} catch (IOException e) {
			throw new RuntimeException();
		} finally{
			try {
				reader.close();
			} catch (IOException e) {
			}
		}
	}
	
	public String getChannel(int code) {
		return code_channel.get(code);
	}
	
	public Integer getCode(String channel) {
		return channel_code.get(channel);
	}
	
}

package my.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Base64;

public class Util {
	
	public static int getProgressPercent(int a, int b) {
		return (int)(a*100.0/b);
	}
	
	public static String addSeparator(String dir) {
		if(!dir.endsWith(File.separator))
			return dir += File.separator;
		else
			return dir;
	}
	
	public static void makeDirs(String file) {
		File f = new File(file);
		if(!f.getParentFile().exists())
			f.getParentFile().mkdirs();
	}

	/**
	 *  将pageNo转成offset
	 * @param pageNo 从1开始
	 * @param pageSize
	 * @return
	 */
	public static int toOffset(int pageNo, int pageSize)	{
		return (pageNo - 1) * pageSize;
	}
	
	/**
	 * @param offset
	 * @param pageSize
	 * @return 起始页为1
	 */
	public static int toPageNo(int offset, int pageSize) {
		return offset/pageSize+1;
	}
	
	public static String cmd(String...cmd) throws IOException	{
		Process process = null;
		BufferedReader br = null;
		ProcessBuilder mBuilder = new ProcessBuilder();
		mBuilder.redirectErrorStream(true);
		StringBuilder sb = new StringBuilder();
		String s;
		try {
			process = mBuilder.command(cmd).start();
			br = new BufferedReader(new InputStreamReader(process.getInputStream(), "utf8"));
			while((s=br.readLine()) != null)
				sb.append(s).append("\n");
			return sb.toString();
		} finally	{
			if(process != null)	process.destroy();
			closeStream(br);
		}
		
	}
	
	public static <T> List<T> subList(List<T> list, int offset, int pageSize) {
		int end = offset + pageSize;
		if(end > list.size()) {
			end = list.size();
		}
		if(offset > list.size()) {
			offset = list.size();
		}
		return list.subList(offset, end);
	}
	
	
	/**
	 * 生成一串随机数组成的字符串. 字母+数字
	 * @param num 随机数的位数
	 * @return
	 */
	public static String randNumChar(int num)	{
		char[] arr = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i=0; i<num; i++)	{
			sb.append(arr[rand.nextInt(arr.length)]);
		}
		return sb.toString();
	}
	
	/**
	 * 生成一串随机数组成的字符串
	 * @param num 随机数的位数
	 * @return
	 */
	public static String randStr(int num)	{
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i=0; i<num; i++)	{
			sb.append("" + rand.nextInt(10));
		}
		return sb.toString();
	}
	
	/**
	 * 是不是手机号. 代替以前的MobileUtil(新手机号判断错误)
	 * @param mobiles
	 * @return
	 */
	public static boolean isMobileNO(String no){
		return no == null ? false : no.matches("1[0-9]{10}");
	}

	/**
	 * 换算ip地址, 规则:
	 * 127.1.2.3
		=127*256*256*256
		+1*256*256
		+2*256
		+3
	 * @param ip
	 */
	public static long convertIp(String ip)	{
		try {
			String[] arr = ip.split("[.]");
			long sum = 0;
			for(int i=0, j=arr.length-1; i<arr.length; i++, j--)	{
				sum += Integer.parseInt(arr[i]) * Math.pow(256, j);
			}
			return sum;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * 关闭多个流
	 * @param closeable
	 */
	public static void closeStream(Closeable...closeable)	{
		for(Closeable c : closeable)	{
			try {
				if(c != null)	c.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static String readTxt(String filePath) {
		InputStream is = null;
		try {
			is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(filePath);
			BufferedReader br = new BufferedReader(new InputStreamReader(is,
					"utf-8"));
			StringBuilder sb = new StringBuilder();
			String s;
			while ((s = br.readLine()) != null) {
				sb.append(s);
			}
			return sb.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				is.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 将String数组转换为Int数组
	 * @param s
	 * @return
	 */
	public static int[] toIntArray(String[] s)	{
		int[] ints = new int[s.length];
		for(int i=0; i<s.length; i++)	{
			ints[i] = Integer.parseInt(s[i]);
		}
		return ints;
	}
	
	/**
	 * 将String数组转换为Int数组
	 * @param s
	 * @return
	 */
	public static long[] toLongArray(String[] s)	{
		long[] longs = new long[s.length];
		for(int i=0; i<s.length; i++)	{
			longs[i] = Long.parseLong(s[i]);
		}
		return longs;
	}
	
	public static List<Integer> toIntList(String[] s)	{
		List<Integer> list = new ArrayList<Integer>();
		for(int i=0; i<s.length; i++)	{
			list.add(Integer.parseInt(s[i]));
		}
		return list;
	}
	
	public static List<Integer> arrToList(int[] s)	{
		List<Integer> list = new ArrayList<Integer>();
		for(int i=0; i<s.length; i++)	{
			list.add(s[i]);
		}
		return list;
	}
	
	/**
	 * 将字符串集合分隔拼接成一条字符串
	 * @param arr
	 * @param splitter
	 * @return
	 */
	public static String join(String[] arr, String splitter)	{
		return join(Arrays.asList(arr), splitter, null);
	}
	public static String join(String[] arr, String splitter, String replacement)	{
		return join(Arrays.asList(arr), splitter, replacement);
	}
	
	/**
	 * 将字符串集合分隔拼接成一条字符串
	 * @param list 集合
	 * @param splitter 分隔符
	 * @return
	 */
	public static String join(List<String> list, String splitter)	{
		return join(list, splitter, null);
	}
	
	/**
	 * 将字符串集合分隔拼接成一条字符串
	 * @param list 集合
	 * @param splitter 分隔符
	 * @param replacement 将与分隔符有冲突的字符串替换
	 * @return
	 */
	public static String join(List<String> list, String splitter, String replacement)	{
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<list.size(); i++)	{
			if(replacement != null && list.get(i) != null)	{
				sb.append(list.get(i).replace(splitter, replacement));
			} else	{
				sb.append(list.get(i));
			}
			if(i < list.size() - 1)	{
				sb.append(splitter);
			}
		}
		return sb.toString();
	}
	
	public static boolean isEmpty(String s)	{
		return s == null || s.length() == 0;
	}
	
	public static boolean isNotEmpty(String s)	{
		return s != null && !"".equals(s);
	}
	
	/**
	 * 把ip4转成long
	 * @param ip4
	 * @return
	 */
	public static long iP2Long(String ip4) {
		String[] ipArray = ip4.split("[.]");
		if (ipArray.length != 4) {
			return 0;
		}
		long ipLong = 0;
		for (int i = 3; i >= 0; --i) {
			long section = Long.valueOf(ipArray[i]);
			ipLong = (ipLong << 8) + section;
		}
		return ipLong;
	}
	
	/**
	 * 把Util.iP2Long(String ip4)的结果还原成IP4字符串
	 * @param ip
	 * @return
	 */
	public static String long2Ip4(long ip)	{
		return new StringBuilder()
			.append(ip & 0xFF).append(".")
			.append((ip >> 8) & 0xFF).append(".")   
			.append((ip >> 16) & 0xFF).append(".")   
			.append((ip >> 24) & 0xFF)
		    .toString();
	}
	
	/**
	 * s是否在list中
	 * @param list
	 * @param s
	 * @return
	 */
	public static boolean contains(List<String> list, String s)	{
		return new HashSet<String>(list).contains(s);
	}
	
	public static boolean contains(String[] list, String s)	{
		return new HashSet<String>(Arrays.asList(list)).contains(s);
	}
	
	public static boolean contains(int[] arr, Integer i) {
		if(i==null) return false;
		for(int o : arr) {
			if(o==i) return true;
		}
		return false;
	}
	
	/**
	 * 把request.getParameterMap()转成字符串, 适用于查看POST参数
	 * @param paramMap
	 * @param charset 是否要把参数urlencode
	 * @return
	 */
	public static String paramMapToString(Map paramMap, String charset)	{
		if(paramMap.size() == 0)	return "";
		StringBuilder sb = new StringBuilder();
		for(Object o : paramMap.entrySet())	{
			Entry e = (Entry)o;
			if(e.getValue() instanceof String[])	{
				for(String val : (String[])e.getValue())	{
					if(charset != null)	{
						try {
							val = URLEncoder.encode(val, charset);
						} catch (UnsupportedEncodingException e1) {
							e1.printStackTrace();
						}
					}
					sb.append(e.getKey()).append("=").append(val).append("&");
				}
			} else {
				String value = (String)e.getValue();
				if(charset != null)	{
					try {
						value = URLEncoder.encode(value, charset);
					} catch (UnsupportedEncodingException e1) {
						e1.printStackTrace();
					}
				}
				sb.append(e.getKey()).append("=").append(value).append("&");
			}
		}
		if(sb.toString().endsWith("&"))	{
			sb.deleteCharAt(sb.length()-1);
		}
		return sb.toString();
	}
	
	public static String paramMapToString(Map paramMap)	{
		return paramMapToString(paramMap, null);
	}
	
	/**
	 * 把a=1&a=2&b=2&c=3这种url参数反解成map
	 * @param queryString
	 */
	public static Map<String,String[]> deserializeQueryStr(String queryString)		{
		if(queryString == null)	throw new IllegalArgumentException("参数不能为空: " + queryString);
		Map<String, String[]> map = new HashMap<String, String[]>();
		for(String s : queryString.split("&"))	{
			try {
				String[] entry = s.split("=", 2);
				if(map.containsKey(entry[0]))	{
					String[] o = map.get(entry[0]);
					String[] n = new String[o.length+1];
					System.arraycopy(o, 0, n, 0, o.length);
					n[o.length] = entry[1];
					map.put(entry[0], n);
				} else	{
					map.put(entry[0], new String[]{entry[1]});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return map;
	}
	
	/**
	 * 从数组里随机筛选一个数
	 * @param arr
	 * @return
	 */
	public static <T> T random(T...arr)	{
		return arr[new Random().nextInt(arr.length)];
	}
	
	public static String md5(byte[] cont){  
	   return hexEncode(md5bytes(cont));
	}
	
	public static byte[] md5bytes(byte[] cont){
		try {  
	        MessageDigest md = MessageDigest.getInstance("MD5");  
	        md.update(cont);  
	        return md.digest();  
	    } catch (NoSuchAlgorithmException e) {  
	        throw new RuntimeException(e); 
		}
	}
	
	public static String md5(File file) throws IOException {  
    	InputStream fis = null;  
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			fis = new FileInputStream(file);  
			byte[] buf = new byte[1024];  
			int len = 0;  
			while ((len = fis.read(buf)) > 0) {  
			    md.update(buf, 0, len);  
			}  
			byte[] bytes = md.digest();
	        return hexEncode(bytes);
        } catch (NoSuchAlgorithmException e1) {
        	throw new RuntimeException(e1);
		} finally	{
			try {
				if(fis != null)	fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }  
	
    public static String hexEncode(byte[] bs) {   
        StringBuilder ret = new StringBuilder(bs.length*2);
        String tmp=null;
        for(byte b:bs){
        	tmp=Integer.toHexString(b&0xff);//don't use String.format(), too slow
        	if(tmp.length()==1) ret.append("0");
        	ret.append(tmp);
        }
        return ret.toString();
    }
    
    public static byte[] decodeHex(String hex) {
    	if(hex.length()%2!=0) 
    		throw new UnsupportedOperationException("hex length must be even number : "+hex);
    	int len = hex.length()/2;
        byte[] ret = new byte[len];
        for (int i=0; i<len; i++) {  
            ret[i] = (byte)Integer.parseInt(hex.substring(i*2, i*2+2),16);  
        }  
        return ret;
    }  
	
	/**
	 * 发起网络请求, 获得本机IP. 
	 * 直接使用InetAddress.getLocalHost()的话, 在服务器上会报UnknownHostException
	 * @return
	 */
	public static String getLocalIp()	  {
	    String localip = null, netip = null;
	    InetAddress ip = null;
	    boolean finded = false;
	    try {
	    	Enumeration netInterfaces = NetworkInterface.getNetworkInterfaces();
	    	while ((netInterfaces.hasMoreElements()) && (!finded)) {
	    		NetworkInterface ni = (NetworkInterface)netInterfaces.nextElement();
	    		Enumeration address = ni.getInetAddresses();
	    		while (address.hasMoreElements()) {
	    			ip = (InetAddress)address.nextElement();
	    			if ((!ip.isSiteLocalAddress()) && (!ip.isLoopbackAddress()) && (ip.getHostAddress().indexOf(":") == -1))	{
	    				netip = ip.getHostAddress();
	    				finded = true;
	    				break;
	    			}
	    			if ((ip.isSiteLocalAddress()) && (!ip.isLoopbackAddress()) && (ip.getHostAddress().indexOf(":") == -1))	{
	    				localip = ip.getHostAddress();
	    			}
	    		}
	    	}
	    } catch (SocketException e) {
	    	e.printStackTrace();
	    }
	    return isNotEmpty(netip) ? netip : localip;
	}
	
	public static String urlEncode(String s, String charset)	{
		try {
			return URLEncoder.encode(s, charset);
		} catch (UnsupportedEncodingException e) {
			return s;
		}
	}
	
	public static String urlDecode(String s, String charset)	{
		try {
			return URLDecoder.decode(s, charset);
		} catch (UnsupportedEncodingException e) {
			return s;
		}
	}
	
	/**
	 * 计算两个时间点相差的天数
	 * @param a
	 * @param b
	 * @return
	 */
	public static int getDiffDay(Date a, Date b)	{
		return (int)((a.getTime() - b.getTime())/1000/3600/24.0);
	}
	
	/**
	 * 从url中截取域名或ip
	 * @param url
	 */
	public static String parseHost(String url)	{
		if(!url.startsWith("http://") && !url.startsWith("https://"))	{
			int slash = url.lastIndexOf("/");
			return slash > -1 ? url.substring(0, slash) : url;
		}
		Matcher m = Pattern.compile("(http://|https://)(.*?)(:|/|$)",Pattern.CASE_INSENSITIVE).matcher(url);
		if(m.find())	{
			return m.group(2);
		}
		return null;
	}
	
	
	public static String getCookie(ServletRequest request, String key) {
		Cookie[] cookies = ((HttpServletRequest) request).getCookies();
		if(cookies != null)	 {
			for(Cookie c : cookies) {
				if(c.getName().equals(key))
					return c.getValue();
			}
		}
		return null;
	}
	
	public static void removeCookie(ServletRequest request, ServletResponse response, 
									String key) {
		Cookie[] cookies = ((HttpServletRequest) request).getCookies();
	    if(cookies != null){
	        for(Cookie cookie : cookies){
	            if(cookie.getName().equals(key)){
	            	cookie.setMaxAge(0);
	            	((HttpServletResponse) response).addCookie(cookie);
	            };
	        }
	    }
	}

	public static String assertNotEmpty(String s) {
		if(Util.isEmpty(s))
			throw new IllegalArgumentException("must not empty:" + s);
		return s;
	}
	
	public static String getSuffix(String filename) {
		if(filename == null)	return null;
		int dot = filename.lastIndexOf('.');
		return dot == -1 ? "" : filename.substring(dot);
	}
	
	public static String trim(String s) {
		return s==null ? null : s.trim();
	}
	
	public static void copyTo(InputStream is, File dest, boolean closeAfterRead) throws IOException {
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		try {
			bis = new BufferedInputStream(is);
			bos = new BufferedOutputStream(new FileOutputStream(dest));
			byte[] buf = new byte[1024];
			int len;
			while((len=bis.read(buf)) != -1) {
				bos.write(buf,0,len);
			}
			bos.flush();
		} finally {
			if(closeAfterRead)
				closeStream(bis, bos);
		}
	}
	
	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 取request ip
	 * @param req
	 * @return
	 */
	//反向代理服务器既有x-forwarded-for, 又有x-real-ip, 根据实际情况取
	public static String getHttpHeadIp(HttpServletRequest request) {  
		// 获取请求主机IP地址,如果通过代理进来，则透过防火墙获取真实IP地址  
		String ip = request.getHeader("X-Forwarded-For");  
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
				ip = request.getHeader("Proxy-Client-IP");  
			}  
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
				ip = request.getHeader("WL-Proxy-Client-IP");  
			}  
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
				ip = request.getHeader("HTTP_CLIENT_IP");  
			}  
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
				ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
			}  
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
				ip = request.getRemoteAddr();  
			}  
		} else if (ip.length() > 15) {  
			String[] ips = ip.split(",");  
			for (int index = 0; index < ips.length; index++) {  
				String strIp = (String) ips[index];  
				if (!("unknown".equalsIgnoreCase(strIp))) {  
					ip = strIp;  
					break;  
				}  
			}  
		} 
		if(ip.equals("0:0:0:0:0:0:0:1")){
			ip="127.0.0.1";
		}
		return ip;
	}
	
	/**
	 * 如果要调用File.mkDirs()时,生成的最高一级目录是什么
	 * @param target
	 * @return 如果父目录都存在,返回null
	 */
	public static File getToCreateDir(File target) {
		String f = target.getParent()+File.separatorChar;
		if(!new File(f).exists()) {
			int index = f.length()-1;
			while(index > 0) {
				if(f.charAt(index) == File.separatorChar) {
					File check = new File(f.substring(0, index));
					if(check.getParentFile().exists()) {
						return check;
					}
				}
				index --;
			}
		}
		return null;
	}
	
	public static String now() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}
	
	public static int yuan2fen(double yuan) {
		return (int)(yuan*100);
	}
	
	public static double fen2yuan(int fen) {
		return fen/100.0;
	}
	
	public static String fmtCurrency(double yuan) {
		return NumberFormat.getCurrencyInstance(Locale.CHINA).format(yuan);
	}
	
	/**
	 * 
	 * @param start 包含
	 * @param end 包含
	 * @return
	 */
	public static int random (int start, int end) {
		return start + new Random().nextInt(end - start + 1);
	}
	
	public static int toIntDate(String date) {
		return Integer.parseInt(date.replace("-", ""));
	}
	
	public static byte[] intToBytes(int i) {  
	    byte[] abyte = new byte[4];  
	    // "&" 与（AND），对两个整型操作数中对应位执行布尔代数，两个位都为1时输出1，否则0。  
	    abyte[0] = (byte) (0xff & i);  
	    // ">>"右移位，若为正数则高位补0，若为负数则高位补1  
	    abyte[1] = (byte) ((0xff00 & i) >> 8);  
	    abyte[2] = (byte) ((0xff0000 & i) >> 16);  
	    abyte[3] = (byte) ((0xff000000 & i) >> 24);  
	    return abyte;  
	}
	
	public static String replaceNotUtf8(String s, String replacement) {	//中文标点也去掉了
		return s.replaceAll( "([\\ud800-\\udbff\\udc00-\\udfff])", replacement);
	}
	
    /** 
     * 将hex字符串转换成字节数组 
     * @param inputString 
     * @return 
     */  
    public static String byte2hex(byte[] b) { // 一个字节的数，  
        StringBuffer sb = new StringBuffer(b.length * 2);  
        String tmp;  
        for (int n = 0; n < b.length; n++) {  
            // 整数转成十六进制表示  
            tmp = (Integer.toHexString(b[n] & 0XFF));  
            if (tmp.length() == 1) {  
                sb.append("0");  
            }  
            sb.append(tmp);  
        }  
        return sb.toString().toUpperCase(); // 转成大写  
    }  
    
    public static byte[] hex2byte(String inputString) {  
        if (inputString == null || inputString.length() < 2) {  
            return new byte[0];  
        }  
        inputString = inputString.toLowerCase();  
        int l = inputString.length() / 2;  
        byte[] result = new byte[l];  
        for (int i = 0; i < l; ++i) {  
            String tmp = inputString.substring(2 * i, 2 * i + 2);  
            result[i] = (byte) (Integer.parseInt(tmp, 16) & 0xFF);  
        }  
        return result;  
    }  
	
	public static byte[] AESEncrypt(byte[] original, SecretKey key) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return cipher.doFinal(original);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	 
	public static byte[] AESDecrypt(byte[] encrypted, SecretKey key) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key); 
            return cipher.doFinal(encrypted);  
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
	public static SecretKey getAESKey(String k){
		try {
			return new SecretKeySpec(k.getBytes("ascii"), "AES");	//只使用可见字符做密码
		} catch (UnsupportedEncodingException e) {
			 throw new RuntimeException(e);
		}
	}
	 
	 /**
	  * 是否中文汉字,不含标点.
	  */
	public static boolean isHanzi(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		return (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS 
//				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
//                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A 
//                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
//                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
		// ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION //中文句号和顿号
				);
    }
	
	
	public static Date getTodayStart(){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE,0);
		c.set(Calendar.SECOND,0);
		c.set(Calendar.MILLISECOND,0);
		return c.getTime();
	}
	
	private static char[] charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
	public static String numericTo62(BigInteger num){
		BigInteger b62 = new BigInteger("62");
		if("0".equals(num))	return "0";
		
		StringBuilder sb = new StringBuilder();
		while(!"0".equals(num.toString()))	{
			BigInteger b = num.divide(b62).multiply(b62); 
		    sb.append(charset[num.subtract(b).intValue()]);
		    num = num.divide(b62);
		}
		return sb.reverse().toString();
	}

	
	public static void main(String[] args) throws UnsupportedEncodingException {
		byte[] b = new byte[]{(byte)0xff,(byte)0xff,(byte)0xff,(byte)0xff};
		System.out.println(byte2hex(b));
		System.out.println(new Base32().encodeAsString(b));
		System.out.println(Base64.encodeBase64URLSafeString(b));
		System.out.println(numericTo62(new BigInteger("ffffffffffffffff", 16)));
		String s = new String(b,"ISO-8859-1");
		System.out.println(s);
	}
	
}

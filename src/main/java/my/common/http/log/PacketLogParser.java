package my.common.http.log;

/**
 * 如何解析request/response消息体. 
 * 当PacketLogParser不为空时, 会记录请求时间到invoke-outline日志中.
 * 根据方法的实现程度,可能会记录到invoke-detail日志中.
 * @author JingYing
 * @date 2014年12月8日
 */
public interface PacketLogParser {
	
	/**
	 * 自定义服务名.如果是udp/tcp服务,推荐写成配置中心的key,方便识别和降级
	 * @return
	 */
	public String getServiceName();
	
	/**
	 * 如何将请求消息体解析成字符串. 通常直接return "some string"即可, 不要让日志解析器进行二次解析. "some string"即要记录的请求体
	 * @param request  注意:http请求下,request为null
	 * @return 需要记录什么请求信息, 一般应提供业务参数串. 返回值会被记录到日志中
	 * @throws Exception
	 */
	public String parseRequest(byte[] request) throws Exception;
	
	/**
	 * 如何将响应的字节流解析成字符串, 以便日志记录.
	 * 由于要多解析一次字节流,所以在讲究性能的地方,不要实现该方法
	 * @param response
	 * @return 需要记录什么响应消息,返回值会被记录到日志中
	 * @throws Exception
	 */
	public String parseResponse(byte[] response) throws Exception;
}

package my.common.http.log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import my.common.Util;

/**
 * LogItem的抽象实现类, 给LogItem子类加入记录的功能
 * @author JingYing 2014-10-13
 *
 */
public abstract class AbstractLogItem implements LogItem {

	public static final Logger 
			log = LoggerFactory.getLogger(AbstractLogItem.class),
			log2 = LoggerFactory.getLogger(AbstractLogItem.class.getName()+".3rdservice-outline"),
			log_error = LoggerFactory.getLogger(AbstractLogItem.class.getName()+".3rdservice-detail-error");
	
	public static final String SPLITTER = "\t";
	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	
	/**
	 * 哪些字符开头的方法需要打印出来
	 */
	public static final String[] WHITELIST = new String[]{
		"com.ted"
	};
	

	/**
	 * 记录日志到本地磁盘或其它地方. 注意性能问题
	 */
	public void log() {
		log2.debug(toAnalysisStr());
		if (hasError()) {
			log.error(toText());
			log_error.error(toText());
		} else {
			log.debug(toText());
		}
	}
	
	private String toAnalysisStr()	{
		List<String> l = new ArrayList<String>();
		l.add(sdf.format(new Date()));
		l.add(getProtocol());
		l.add(getServiceUrl());
//		l.add(getRequestId()+" " + getWid());
		//l.add(getWid()+"");
		l.add(getCostMillis()+"");
		l.add(hasError() ? "1" : "0");
		if(!getParticularProp().isEmpty())	{
			l.addAll(getParticularProp());
		}
		return Util.join(l, SPLITTER, "$$");
	}
	
	protected List<String> filterInvokeStack()	{
		List<String> list = new ArrayList<String>();
		if(getInvokeStack() != null)	{
			for(StackTraceElement ele : getInvokeStack())	{
				for(String s : WHITELIST)	{
					if(ele.toString().startsWith(s))	{
						list.add(ele.toString());
					}
				}
			}
		}
		return list;
	}
	
	@Override
	public String getRequestId()	{
		return "";
	}
}

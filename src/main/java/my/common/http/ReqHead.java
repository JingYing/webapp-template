package my.common.http;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * http请求头
 */
public class ReqHead {
	
	private Map<String,String> kv = new HashMap<>();
	
	/**
	 * 转换成http协议可识别的请求头
	 * @return
	 */
	public Map<String, String> toMap()	{
		for(Iterator<Entry<String,String>> it=kv.entrySet().iterator(); it.hasNext();) {
			if(it.next().getValue()==null)
				it.remove();
		}
		return kv;
	}
	
	public ReqHead set(String key, String value) {
		kv.put(key, value);
		return this;
	}

	public ReqHead setHost(String host) {
		kv.put("Host", host);
		return this;
	}

	public ReqHead setContentType(String contentType) {
		kv.put("Content-Type", contentType);
		return this;
	}

	public ReqHead setReferer(String referer) {
		kv.put("Referer", referer);
		return this;
	}

	public ReqHead setUserAgent(String userAgent) {
		kv.put("User-Agent", userAgent);
		return this;
	}

	public ReqHead setCookie(String cookie) {
		kv.put("Cookie", cookie);
		return this;
	}

	public String getHost() {
		return kv.get("Host");
	}

	public String getContentType() {
		return kv.get("Content-Type");
	}

	public String getReferer() {
		return kv.get("Referer");
	}

	public String getUserAgent() {
		return kv.get("User-Agent");
	}

	public String getCookie() {
		return kv.get("Cookie");
	}

}

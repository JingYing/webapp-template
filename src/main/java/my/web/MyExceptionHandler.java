 package my.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MissingServletRequestParameterException;

public class MyExceptionHandler {
	static Logger log = LoggerFactory.getLogger(MyExceptionHandler.class);
	
    public static String handleApiException(Exception e) {
    	log.error(e.getMessage(), e);
    	if(e instanceof MissingServletRequestParameterException) {
    		return new Out().setErr(400).toString(); //隐藏springmvc默认的参数无值提示
    	} else if(e instanceof IllegalArgumentException) {
    		return new Out().setErr(400).setMsg(e.getMessage()).toString(); 
    	} else if(e instanceof SignException) {
    		return new Out().setErr(401).setMsg(e.getMessage()).toString(); 
    	} else if(e instanceof IllegalTokenException) {
    		return new Out().setErr(402).setMsg(e.getMessage()).toString(); 
    	} else {
    		return new Out().setErr(500).setMsg(e.getMessage()).toString();
    	}
    }
  
}  
package my.web;
public class IllegalTokenException extends Exception {

		public IllegalTokenException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public IllegalTokenException(String message, Throwable cause, boolean enableSuppression,
				boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
			// TODO Auto-generated constructor stub
		}

		public IllegalTokenException(String message, Throwable cause) {
			super(message, cause);
			// TODO Auto-generated constructor stub
		}

		public IllegalTokenException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}

		public IllegalTokenException(Throwable cause) {
			super(cause);
			// TODO Auto-generated constructor stub
		}
	}
package my.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public class WebUtil {
	
	public static HttpEntity<InputStreamResource> createResponse(File file) throws IOException {
		return createResponse(new FileInputStream(file), file.getName(), file.length());
	}
	
	public static HttpEntity<InputStreamResource> createResponse(InputStream is, String fileName, long length)  {
		HttpHeaders header = new HttpHeaders();
		try {
			fileName = new String(fileName.getBytes(), "ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
		}
		header.set("Content-Disposition", "attachment; filename=" + fileName);
		header.setContentLength(length);
		return new HttpEntity<InputStreamResource>(new InputStreamResource(is), header);
	}

}

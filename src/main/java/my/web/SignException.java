package my.web;

public class SignException extends RuntimeException {

	public SignException() {
		super();
	}

	public SignException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SignException(String message, Throwable cause) {
		super(message, cause);
	}

	public SignException(String message) {
		super(message);
	}

	public SignException(Throwable cause) {
		super(cause);
	}
}

package my.web;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 向前台输出的标准json格式
 */
public class Out  {
	private transient static Gson gson = new GsonBuilder().serializeNulls().disableHtmlEscaping().create();//防止该成员变量被json序列化

	private int err = 0;
	private String msg;
	private JsonElement data = new JsonObject();
	
	/**
	 * 重要.不要随便修改
	 */
	@Override
	public String toString()	{
		return gson.toJson(this);	
	}
	
	public Out setMsg(String msg) {
		this.msg = msg;
		return this;
	}
	
	public Out setErr(int err) {
		this.err = err;
		return this;
	}
	
	public Out setData(JsonElement data) {
		this.data = data;
		return this;
	}
	
	public JsonElement toJsonTree()	{
		return gson.toJsonTree(this);
	}
	
	public static void main(String[] args) {
		System.out.println(new Out().toString());
	}
}
